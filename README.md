# EL BARATÓN

¿Ya sabes como hacer tu mercado en línea, recibirlo en la puerta de tu casa y sin tener que pagar más? Visita nuestro sitio web *_El Baratón_*, y no dejes que nadie te saque de tu sofa.

### CONOCE EL PROYECTO

#### La razón de usar Reactjs

Este es el demo de *_El Baratón_* hecho en Reactjs de facebook, es una aplicación de una sola página, más poderoso y conveniente que otros métodos para que el envío de datos a través de _http Request_ que recargan nuevamente el _DOM_, Reactjs nos permite actualizar la información mostrada al usuario a través de un _Virtual DOM_ evitando que tengamos que recargar nuevamente la página

#### Que es El Baratón

Esta es una prueba de desarrollo en front-end, el objetivo era crear una aplicación que permitiera realizar las tareas más comúnes a la hora de comprar en una tienda en linea, como navegar a través de las categorias de productos, visualar información sobre cada producto como su precio, cantidades en invetario, agregar nuevos productos a la bolsa entre otros.

#### Metodología de trabajo

##### Layouts

![alt text](https://gitlab.com/clovisrodriguez/elbaraton/blob/master/src/common/layout1.jpg)
![alt text](https://gitlab.com/clovisrodriguez/elbaraton/blob/master/src/common/layout2.jpg")

Primero lo primero *Planear*, cuando no se planea se empieza a improvisar y a pesar de que existen frameworks como bootstrap que pueden ayudarnos a prototipar rápidamente layouts bellos, _SI NO SE PLANEA SE IMPROVISA_ y me ha pasado que los diseños se ven "perezosos".

Elaborar una referencia visual me ayuda a identificar los componentes, como se relacionan y que no se me olvide incluir elementos críticos en el funcionamiento de la aplicación.

##### Set Up!

Realizado los layouts, preparo el entorno de desarrollo para correr el proyecto de forma local y automatizaciones que mejoran el proceso de testing

##### Programación

*_Crear componentes contenedores:_* inicio por constuir las jerarquías desde el componente contenedor hasta los elementos más pequeños como botones o espacios para los textos y valores

*_Comunicar componentes:_* Agregar los componentes a los metodos render() de su _parent_

*_Construir métodos de comunicación:_* Esta etapa la trabajo con una lista de funciones y aplicaciones que se espera que realice la aplicación construyendo metodos que permitan leer elementos almacenados en las bases de datos, usarlos para renderizar elementos en el DOM, guardar información entre otras.

#### Como Instalar y Correr

Dependiendo del sistema operativo pueden cambiar los comandos

Primero Instalar [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), si estas en un sistema operativo de linux seguramente ya la tienes instalada, puedes veríficar la instalación `git --version`. Una vez instalado git crea una carpeta donde vayas a ubicar el proyecto y correo el siguiente comando

```bash
git clone git@gitlab.com:clovisrodriguez/elbaraton.git
```

Este comando crea una copia los archivos del repositorio directamente en tu máquina, una vez tengas los archivos es importante que tengas instalado [nodeJS](https://nodejs.org/es/download/) y npm.

ya con estas dos cosas es más fácil iniciar una aplicación puedes usar el siguiente comando

```bash
npm install
```

Este comando instalara todas las dependencias necesarias para correr el proyecto que se encuentran en package.json, una vez instaladas todas las dependencias corre este último comando que inicia un servidor local en el que podras visualizar la aplicación.

```bash
npm start
```

##### Algunas fallas que pueden presentarse

Puede que después de npm install algunas dependecias no esten actualidas o haga falta instalarlas manualmente de todas formas, la consola debería indicarte que paquete esta haciendo falta.

###### Si vas a modificar el CSS no olvides correr gulp

Los estilos estan escritos en SCSS y para compilar utilizo gulp y otras dependecias de este, si no corres gulp no compilaran y no se reflejaran los estilos.

## Al equipo de Rappi

Espero que la prueba no solo muestre la cálidad del trabajo que puedo hacer que estoy seguro que con la guía adecuada cada día puede ser mejor, sino que les ayudé a conocer mi proceso como profesional y persona, mi forma de planear, registrar el trabajo, comunicar los retos, resultados y lo que estoy intentando realizar.

De darme la oportunidad de trabajar con ustedes no esta de más decir que deseo dar lo mejor de mí, para aprender, proponer para contribuir a crecer su familia de la que espero ser parte, espero escuchar pronto de ustedes.

Saludos
