import React, { Component } from 'react';

class DisplayCategories extends Component {
  constructor(props){
    super(props);
    this.categoryWasClicked = this.categoryWasClicked.bind(this);
  }

  categoryWasClicked(event){
    event.preventDefault()
    const {index} = this.props
    const {updatePostList} = this.props
    return updatePostList(event,index)
  }

  render() {
    const {post} = this.props
    const {index} = this.props
    return (
      <div className="col-lg-3 col-md-4 col-sm-6">
        <div onClick={this.categoryWasClicked} className="category">
          <h3>{post.name}</h3>
        </div>
      </div>
    );
  }
}

export default DisplayCategories;
