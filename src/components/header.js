import React, { Component } from 'react';
import logo from '../logo.svg'

class Header extends Component {
  constructor(props){
    super(props);
    this.itemWasClicked = this.itemWasClicked.bind(this);
  }

  itemWasClicked(event){
    event.preventDefault()
    const {updateShowMenu} = this.props
    return updateShowMenu(event)
  }

  render() {
    const {showMenu} = this.props
    return (
      <header>
        <div className ="container">
          <div className = "row">
            <img className="col-lg-2" src={logo} alt="logo de el baraton" />
            <ul className="offset-lg-8 col-lg-2" id="nav-menu">
              <li><i onClick={this.itemWasClicked} id='filter' className={showMenu === 'filter' ? ' fas fa-filter fa-2x toggle' : 'fas fa-filter fa-2x '}></i></li>
              <li><i onClick={this.itemWasClicked} id='shopping-bag' className={showMenu === 'shopping-bag' ? ' fas fa-shopping-bag fa-2x toggle' : 'fas fa-shopping-bag fa-2x '}></i></li>
            </ul>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
