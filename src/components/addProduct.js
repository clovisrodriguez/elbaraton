import React, { Component } from 'react';

//components
import PostProducts from '../data/products'

class AddProduct extends Component {
  constructor(props){
    super(props);
    this.changeInventory = this.changeInventory.bind(this)
    this.addWasClicked = this.addWasClicked.bind(this)
    this.state = {
      productWasAdded: []
    }
  }

  changeInventory(event){
    const {updateInventory} = this.props
    updateInventory(event)
  }

  addWasClicked(name,quantity,price){
    const {listenAddWasClicked} = this.props
    const {closeProducts} = this.props
    const total = quantity * Number(price.replace(/[^0-9.-]+/g,""))
    alert("tu producto fue añadido")
    let sendProduct = {
      "name": name,
      "price" : price,
      "quantity" : quantity,
      "total" : total
    }
    listenAddWasClicked(sendProduct)
    closeProducts()
  }

  render() {
    const {addNewProduct} = this.props
    const {newProduct} = this.props

    let renderItem = PostProducts.products.filter(
      (product) => {
        return product.id === newProduct;
      }
    );
    const {closeProducts} = this.props
    const {inventory} = this.props
    return (
      <div>
        { addNewProduct ? (
          <div id="productOpen">
          {renderItem.map((postDetail) =>{
            return (
              <div className="modulecontent" key={postDetail.id} name={postDetail.name}>
                <div className="banner">
                  <p onClick={closeProducts} style={{cursor:'pointer'}}>[x]</p>
                  <h3>{postDetail.name}</h3>
                </div>
                <div>
                  <table className="table">
                    <tbody>
                      <tr>
                        <td>Precio</td>
                        <td>{postDetail.price}</td>
                      </tr>
                      <tr>
                        <td>Cantidad</td>
                        <td><input type="number" min="0" max={postDetail.quantity} onChange={this.changeInventory} defaultValue="1" /></td>
                      </tr>
                      <tr>
                        <td>Total</td>
                        <td>{'$ '+(Number(postDetail.price.replace(/[^0-9.-]+/g,"")) * inventory).toFixed(0)}</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td><button className="elbaraton-button" onClick={() => this.addWasClicked(postDetail.name,inventory,postDetail.price)}>añadir</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            )
          })}
        </div>) : ""}
      </div>
    );
  }
}

export default AddProduct;
