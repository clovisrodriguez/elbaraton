import React, { Component } from 'react';

class MenuArea extends Component {
  constructor(props){
    super(props);
    this.closeWasClicked = this.closeWasClicked.bind(this);
    this.removeWasClicked = this.removeWasClicked.bind(this);
    this.purchaseMadeListener = this.purchaseMadeListener.bind(this)
  }

  closeWasClicked(event){
    event.preventDefault()
    const {updateShowMenu} = this.props
    const {shopBag} = this.props
    return updateShowMenu(event)
  }

  removeWasClicked(event){
    event.preventDefault
    const {removeItemShopBag} = this.props
    return removeItemShopBag(event)
  }

  purchaseMadeListener(event){
    event.preventDefault
    const {purchaseMade} = this.props
    return purchaseMade(event)
  }

  render() {

    let renderMenu
    const {shopBag} = this.props
    const {showMenu} = this.props


    if(showMenu === 'shopping-bag'){
      renderMenu=(
          <table className="table" id="shopBag-table">
            <thead>
              <td onClick={this.closeWasClicked} style={{cursor:'pointer'}} id=''>[x]</td>
            </thead>
          {shopBag.map((item,index)=>{
            return (
              <tbody key={index}>
                <tr>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td className="removeItem" onClick={this.removeWasClicked}>eliminar</td>
                </tr>
                <tr>
                  <td>x</td>
                  <td>{item.quantity}</td>
                </tr>
              </tbody>
            )
          })}
          <tbody>
            <tr>
              <th>Total</th>
              <th>{'$ ' + shopBag.reduce(function(prev, cur) { return prev + cur.total;},0)}</th>
            </tr>
            <tr>
              <td></td>
              <td><button className="elbaraton-button" onClick={this.purchaseMadeListener}>Comprar</button></td>
            </tr>
          </tbody>
        </table>
      )
    } else if(showMenu === 'filter') {
      renderMenu=(
        <table className='table' id="filter-table">
          <thead>
            <td onClick={this.closeWasClicked} style={{cursor:'pointer'}} id=''>[x]</td>
          </thead>
          <tbody>
            <tr>
              <th>Disponiblidad</th>
            </tr>
            <tr>
              <td>
                <select>
                  <option>disponible</option>
                  <option>agotado</option>
                </select>
              </td>
            </tr>
            <tr>
              <th>Precio</th>
            </tr>
            <tr>
              <td>
                <input type="range" name="$" min="0" max="100000" value="1000" />
              </td>
            </tr>
            <tr>
              <th>Inventario</th>
            </tr>
            <tr>
              <td>
                <input type="range" name="$" min="0" max="1000" value="200" />
              </td>
            </tr>
          </tbody>
        </table>
      )
    }

    return (
      <div id="menuArea" className={showMenu !== '' ? 'toggle' : ''}>
        {renderMenu}
      </div>
    );
  }
}

export default MenuArea;
