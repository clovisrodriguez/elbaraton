import React, { Component } from 'react';

class DisplayProducts extends Component {
  constructor(props){
    super(props);
    this.openProduct = this.openProduct.bind(this)
  }

  openProduct(event){
    const {post} = this.props
    const {updateAddNewProduct} = this.props
    updateAddNewProduct(true,post.id)
  }
  render() {
    const {post} = this.props
    return (
      <div className="col-lg-3 col-md-4 col-sm-6">
        <div onClick={this.openProduct} className="product">
          {post.available === false ? <h3>AGOTADO</h3>: <h3>DISPONIBLE</h3>}
          <div>
            <h4>{post.name}</h4>
            <p>Precio: {post.price}</p>
            <p>Cantidad: {post.quantity}</p>
          </div>
        </div>
      </div>
    )
  }
}

export default DisplayProducts;
