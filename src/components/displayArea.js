import React, { Component } from 'react';
import PostProducts from '../data/products.json';
import PostCategories from '../data/categories.json';

// components
import DisplayProducts from '../components/displayProducts'
import DisplayCategories from '../components/displayCategories'
import AddProduct from '../components/addProduct';

class DisplayArea extends Component {
  constructor(props){
    super(props);
    this.updatePostList = this.updatePostList.bind(this)
    this.renderCondition = this.renderCondition.bind(this)
    this.initPostList = this.initPostList.bind(this)
    this.backPostList = this.backPostList.bind(this)
    this.updateAddNewProduct = this.updateAddNewProduct.bind(this)
    this.closeProducts = this.closeProducts.bind(this)
    this.updateInventory = this.updateInventory.bind(this)
    this.listenAddWasClicked = this.listenAddWasClicked.bind(this)
    this.state = {
      postList: [],
      backList:[],
      categoryId: [],
      available: false,
      conditionVef: [],
      newProduct: ['foo','balance','force'],
      addNewProduct: false,
      inventory: 1
    }
  }

  initPostList(){
    this.setState({postList: PostCategories.categories})
  }

  backPostList(){
    this.setState({postList: this.state.backList})
  }

  updatePostList(event,index){
    let currentPostList = this.state.postList
    this.setState({backList:currentPostList})
    this.setState({postList: currentPostList[index].sublevels})
    this.setState({categoryId: currentPostList[index].id})
    this.setState({conditionVef: currentPostList[index]})
  }

  updateAddNewProduct(response,product){
    this.setState({newProduct: product})
    this.setState({addNewProduct:response})
  }

  closeProducts(){
    this.setState({addNewProduct:false})
  }

  updateInventory(event){
    this.setState({inventory:event.target.value})
    console.log(this.state.inventory)
  }

  listenAddWasClicked(object){
    const {updateShopBag} = this.props
    updateShopBag(object)
  }

  renderCondition(){
    if(this.state.postList === PostCategories.categories){
      return true
    } else if (this.state.conditionVef.hasOwnProperty('sublevels')) {
      return true
    } else {
      return false
    }
  }

  componentDidMount(){
    this.setState({
      postList: PostCategories.categories
    })
    this.setState({
      newProduct: null
    })
  }

  render() {
    let filteredProducts = PostProducts.products.filter(
      (product) => {
        return product.sublevel_id === this.state.categoryId;
      }
    );



    let renderArea = this.renderCondition() ? this.state.postList.map((category,index) =>
      <DisplayCategories updatePostList={this.updatePostList} post={category} key={category.id} index={index} />
    ) :
    filteredProducts.map((item) =>
      <DisplayProducts updateAddNewProduct={this.updateAddNewProduct} post={item} key={item.id} />
    )

    const {addNewProduct} = this.props
    const {showMenu} = this.props
    const {postList} = this.state
    return (
      <div id="displayArea" className={showMenu !== '' ? 'toggle' : ''}>
        <div  className="container">
          <div className="row" id="nav-displayArea">
            <p onClick={this.initPostList} style={{cursor:'pointer'}}>Inicio</p>
            <p onClick={this.backPostList} style={{cursor:'pointer'}}>Volver</p>
            <label style={{cursor:'pointer'}}>Organizar por
              <select>
                <option>Precio</option>
                <option>Orden Alfabético</option>
              </select>
            </label>
          </div>
          <div className ="row">
            {renderArea}
          </div>
        </div>
        <AddProduct
          updateInventory={this.updateInventory}
          closeProducts={this.closeProducts}
          addNewProduct={this.state.addNewProduct}
          newProduct={this.state.newProduct}
          inventory={this.state.inventory}
          listenAddWasClicked={this.listenAddWasClicked}/>
      </div>
    );
  }
}

export default DisplayArea;
