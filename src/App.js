import React, { Component } from 'react';
import './css/default.css';

// components
import Header from './components/header';
import DisplayArea from './components/displayArea';
import MenuArea from './components/menuArea';

class App extends Component {
  constructor(props){
    super();
    this.updateShowMenu = this.updateShowMenu.bind(this);
    this.updateShopBag = this.updateShopBag.bind(this);
    this.removeItemShopBag = this.removeItemShopBag.bind(this);
    this.purchaseMade = this.purchaseMade.bind(this);
    this.state = {
      showMenu: '',
      shopBag: []
    }
  }
  updateShowMenu(event) {
    this.setState({showMenu: event.target.id})
  }

  updateShopBag(item){
    let CurrentshopBag = this.state.shopBag
    CurrentshopBag.push(item)
    this.setState({shopBag : CurrentshopBag})
  }

  removeItemShopBag(event){
    let CurrentshopBag = this.state.shopBag
    CurrentshopBag.pop(event)
    this.setState({shopBag : CurrentshopBag})
  }

  purchaseMade(event){
    alert('gracias por tu compra')
    this.setState({shopBag: []})
  }

  render() {
    return (
      <div className="App">
        <Header updateShowMenu={this.updateShowMenu}  showMenu={this.state.showMenu}/>
        <section id="wrapper" className={this.state.showMenu !== '' ? 'toggle' : ''}>
          < MenuArea
            shopBag={this.state.shopBag}
            showMenu={this.state.showMenu}
            updateShowMenu={this.updateShowMenu}
            removeItemShopBag={this.removeItemShopBag}
            purchaseMade={this.purchaseMade}/>
          < DisplayArea updateShopBag={this.updateShopBag} showMenu={this.state.showMenu}/>
        </section>
      </div>
    );
  }
}

export default App;
